package com.example.inventorynithya;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.view.View;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.inventorynithya.database.DatabaseClient;
import com.example.inventorynithya.model.User;


public class Register extends AppCompatActivity {
    TextView question2;
    // Add button Move to next Activity and previous Activity

    EditText etUserName;
    EditText etName;
    EditText etPassword;
    EditText etConfirmPassword;
    Button register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        etUserName = findViewById(R.id.username);
        etName = findViewById(R.id.name);
        etPassword = findViewById(R.id.password);
        etConfirmPassword = findViewById(R.id.confirm_password);
        register = (Button) findViewById(R.id.register_btn);
        question2 = (TextView) findViewById(R.id.regitertext);
        question2.setText("Register Here");

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataEntered();
            }
        });


//        register.setOnClickListener(v -> {
//            // Intents are objects of the android.content.Intent type. Your code can send them to the Android system defining
//            // the components you are targeting. Intent to start an activity called oneActivity with the following code
//            Intent intent = new Intent(Register.this, MainActivity.class);
//            // start the activity connect to the specified class
//            startActivity(intent);
//        });
    }
    boolean isEmpty(EditText text) {
        CharSequence str = text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    void checkDataEntered() {
        String username= etUserName.getText().toString();
        String name= etName.getText().toString();
        String password=etPassword.getText().toString();
        String confirmpassword= etConfirmPassword.getText().toString();


        if (username.isEmpty()) {
            Toast t = Toast.makeText(this, "You must enter user name to register!", Toast.LENGTH_SHORT);
            t.show();
        }

       else if (name.isEmpty()) {
            etName.setError("name is required!");
        } else if (password.isEmpty()) {
            etPassword.setError("name is required!");


        } else if (confirmpassword.isEmpty()) {
            etPassword.setError("name is required!");


        } else if (!password.equals(confirmpassword)) {
            etPassword.setError("name is required!");

        }
       else {
            User user =new User();
            user.setUserName(username);
            user.setName(name);
            user.setPassword(password);
           createTask(user);

        }


    }


    private void createTask(User user) {
        class saveTaskInBackend extends AsyncTask<Void, Void, Void> {
            @SuppressLint("WrongThread")
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .dataBaseAction()
                        .insertDataIntoTaskList(user);

                return null;
            }

            @Override
            protected void onPostExecute(Void user) {
                super.onPostExecute(user);
                Toast t = Toast.makeText(getApplicationContext(), "Registered!", Toast.LENGTH_SHORT);
                t.show();
            }
        }
        saveTaskInBackend st = new saveTaskInBackend();
        st.execute();
    }
}