package com.example.inventorynithya.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


import com.example.inventorynithya.model.User;

import java.util.List;

@Dao
public interface OnDataBaseAction {

    @Query("SELECT * FROM User")
    List<User> getAllTasksList();

    @Query("DELETE FROM User")
    void truncateTheList();

    @Insert
    void insertDataIntoTaskList(User task);

    @Query("DELETE FROM User WHERE userName = :userName")
    void deleteTaskFromId(String userName);

    @Query("SELECT * FROM User WHERE userName = :userName")
    User selectDataFromAnId(String userName);

//    @Query("UPDATE User SET taskTitle = :taskTitle, taskDescription = :taskDescription, date = :taskDate, " +
//            "lastAlarm = :taskTime, event = :taskEvent WHERE taskId = :taskId")
//    void updateAnExistingRow(int taskId, String taskTitle, String taskDescription , String taskDate, String taskTime,
//                             String taskEvent);

}
