package com.example.inventorynithya;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.view.View;
import android.text.TextUtils;
import android.widget.Toast;
import android.widget.DatePicker;

import java.util.Calendar;

public class Menu1grn extends AppCompatActivity {
    EditText etDate;
    EditText etGrnNumber;
    EditText etItemName;
    EditText etItemNumber;
    Button addGrn;
    String dateGet;
    private Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu1grn);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etDate = findViewById(R.id.date);
        etGrnNumber = findViewById(R.id.grn_number);
        etItemName = findViewById(R.id.item_name);
        etItemNumber = findViewById(R.id.item_number);
        addGrn = (Button) findViewById(R.id.add_grn);
        calendar = Calendar.getInstance();

        Intent intent = getIntent();
        String message = intent.getStringExtra("userId");


        etDate.setOnClickListener(view -> {
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(Menu1grn.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            dateGet = dayOfMonth +"-"+ month+ "-" +year;
                            etDate.setText(dateGet);
                        }
                    }, year, month, dayOfMonth);




            datePickerDialog.show();

        });


    }
}