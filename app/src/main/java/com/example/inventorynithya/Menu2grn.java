package com.example.inventorynithya;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.EditText;
import android.view.View;
import android.text.TextUtils;
import android.widget.Toast;

public class Menu2grn extends AppCompatActivity {
    String[] mobileArray = {"GRN12902","GRN54671","GRN54679","GRN33678",
            "GRN24672","GRN44367","GRN54656","MGRN54676"};
    Button addGrn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu2grn);
        addGrn = (Button) findViewById(R.id.addGrn);
        addGrn.setOnClickListener(view -> {
            Intent intent = new Intent(Menu2grn.this, Menu1grn.class);
            startActivity(intent);

        });

        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.activity_list_item, mobileArray);

        ListView listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(adapter);
    }
}