package com.example.inventorynithya;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

import com.example.inventorynithya.database.DatabaseClient;
import com.example.inventorynithya.model.User;

public class MainActivity extends AppCompatActivity {
    // define the global variable
    TextView question1;
    EditText etUsername;
    EditText etPassword;
    // Add button Move to Activity
    Button goRegister;
    Button registerGo;
    String userName = "";
    String password = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        goRegister = (Button) findViewById(R.id.login_btn);
        registerGo = (Button) findViewById(R.id.goto_register);

        question1 = (TextView) findViewById(R.id.textview);
        etUsername = findViewById(R.id.editText);
        etPassword = findViewById(R.id.editText2);
        question1.setText("Login");

        registerGo.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, Register.class);
            startActivity(intent);
        });

        goRegister.setOnClickListener(v -> {
            userName= etUsername.getText().toString();
            password= etPassword.getText().toString();

            if (userName.isEmpty())
            {
          etUsername.setError("User name required");
            }
            else if (password.isEmpty()){
                etPassword.setError("Password required");

            }
            else {

                createTask();

            }

        });
    }

    private void createTask() {
        class saveTaskInBackend extends AsyncTask<Void, Void, User> {
            @SuppressLint("WrongThread")
            @Override
            protected User doInBackground(Void... voids) {
                User user = DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .dataBaseAction().selectDataFromAnId(userName);

                return user;
            }

            @Override
            protected void onPostExecute(User user) {
                super.onPostExecute(user);
                if (user != null) {
                    if(user.getPassword().equals(password)){
                        Toast t = Toast.makeText(getApplicationContext(), "Logged in!", Toast.LENGTH_SHORT);
                        t.show();
                        Intent intent = new Intent(MainActivity.this, Menu2grn.class);
                        intent.putExtra("userId", user.getUserName());

                        startActivity(intent);
                    }else {
                        Toast t = Toast.makeText(getApplicationContext(), "Password error", Toast.LENGTH_SHORT);
                        t.show();
                    }

                } else {
                    Toast t = Toast.makeText(getApplicationContext(), "something error", Toast.LENGTH_SHORT);
                    t.show();


                }
            }
        }
        saveTaskInBackend st = new saveTaskInBackend();
        st.execute();
    }
}

