package com.example.inventorynithya.model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Inventory implements Serializable {


    @PrimaryKey(autoGenerate = true)
    int grnId;

    @ColumnInfo(name = "grnDate")
    String grnDate;
    @ColumnInfo(name = "grnNumber")
    String grnNumber;
    @ColumnInfo(name = "grnItemName")
    String grnItemName;
    @ColumnInfo(name = "grnItemNumber")
    String grnItemNumber;
    public Inventory() {

    }
    public int getGrnId() {
        return grnId;
    }

    public void setGrnId(int grnId) {
        this.grnId = grnId;

    }

    public String getGrnDate() {
        return grnDate;
    }

    public void setGrnDate(String grnDate) {
        this.grnDate = grnDate;
    }

    public String getGrnNumber() {
        return grnNumber;
    }

    public void setGrnNumber(String grnNumber) {
        this.grnNumber = grnNumber;
    }

    public String getGrnItemName() {
        return grnItemName;
    }

    public void setGrnItemName(String grnItemName) {
        this.grnItemName = grnItemName;
    }

    public String getGrnItemNumber() {
        return grnItemNumber;
    }

    public void setGrnItemNumber(String grnItemNumber) {
        this.grnItemNumber = grnItemNumber;
    }
}
